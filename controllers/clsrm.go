package controllers

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	// "github.com/jackc/pgx/v4"
)

var TableName = "classroom"

type Claz struct {
	Id      int32    `json:"id"`
	Name    string `json:"name"`
	Address string `json:"address"`
}

func Displayall(c *gin.Context) {
	var allStudDetails []Claz
	log.Println("reached stage 1")
	qry := fmt.Sprintf("SELECT * FROM %s", TableName)
	log.Println("reached stage 2")

	rows, err := dbConnect.Query(context.Background(), qry)
	log.Println("reached stage 3")

	if err != nil {
		log.Println("error while displaying")
		log.Println(err)

	}
	log.Printf("reached stage 3.5")
	for rows.Next() {
		log.Println("reached stage 4")
		value, err := rows.Values()

		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"message": "no record found"})
			log.Println(err)
		}
		log.Println("reached data entry")
		rid := value[0].(int32)
		sname := value[1].(string)
		saddress := value[2].(string)

		allStudDetails = append(allStudDetails, Claz{
			Id:      rid,
			Name:    sname,
			Address: saddress,
		})
	}

	defer rows.Close()
	if allStudDetails == nil {
		log.Println("there is nothing in it")
	}
	c.JSON(http.StatusOK, gin.H{"message": "records found", "records": allStudDetails})

}


func DisplayOne(c *gin.Context){
	var detailsOne Claz
	enteredId:=c.Param("testId")
	log.Println("displaying one -1")
	qry:= fmt.Sprintf("SELECT * FROM %s WHERE id=%v",TableName,enteredId)
	log.Println("displaying one -2 ")
	
	row,err:=dbConnect.Query(context.Background(),qry)
	log.Printf("displaying one -3 %v",row)
	
	if err!=nil{
		log.Println("error occured while executing query")
		log.Println(err)
		return 
	}
	log.Println("displaying one -4")

	
	for row.Next(){
		
		value,err:=row.Values()
		if err!=nil{
			log.Println(err)
			log.Println("error in extracting values from row")
		}
	
		detailsOne.Id=value[0].(int32)
		detailsOne.Name=value[1].(string)
		detailsOne.Address=value[2].(string)
	}

	c.JSON(http.StatusOK,gin.H{
		"message":"record found",
		"record":detailsOne,
	})
	
}
type InsertType struct{
	Id      string   `json:"id"`
	Name    string `json:"name"`
	Address string `json:"address"` 
}
func InsertOne(c *gin.Context){
	var unoInsert InsertType
	unoInsert.Id=c.PostForm("id")
	unoInsert.Name=c.PostForm("name")
	unoInsert.Address=c.PostForm("address")
	// most common error while sending request is error:column not found
	// resolve it by putting name and address in sign quotes('<text>')
	
	
	qry:=fmt.Sprintf("INSERT INTO %s(id,name,address) VALUES(%s,%s,%s)",TableName,unoInsert.Id,unoInsert.Name,unoInsert.Address)
	
	_,err:=dbConnect.Exec(context.Background(),qry)
	if err!=nil{
		log.Println(err)
		
	}
	c.JSON(http.StatusOK,gin.H{
		"message":"insert one is working",
		"record":unoInsert,
	})
	
	}

func UpdateOne(c *gin.Context){
	var updDetails InsertType
	updDetails.Id=c.PostForm("id")
	updDetails.Name=c.PostForm("name")
	updDetails.Address=c.PostForm("address")
	log.Println(updDetails)

	
	
	qry:=fmt.Sprintf("UPDATE %v SET name=%s, address=%s WHERE id=%s",TableName,updDetails.Name,updDetails.Address,updDetails.Id)

	
	_,err:=dbConnect.Exec(context.Background(),qry)

	
	if err!=nil{

		log.Println(err)

		c.JSON(http.StatusBadRequest,gin.H{
			"message":"can't update because the id doesn't exist",
			"error":err,
		})
	}
	c.JSON(http.StatusOK,gin.H{
		"message":"update one is working",
		"record":updDetails,
	})

}

func DeleteOne(c *gin.Context){
	var deleteOne InsertType
	deleteOne.Id=c.PostForm("id")
	deleteOne.Name=c.PostForm("name")
	deleteOne.Address=c.PostForm("address")
	log.Println(deleteOne)

	qry:=fmt.Sprintf("DELETE FROM %s WHERE id=%s",TableName,deleteOne.Id)

	_,err:=dbConnect.Exec(context.Background(),qry)

	if err!=nil{
		log.Println(err)
	}
	c.JSON(http.StatusAccepted,gin.H{
		"message":"record deleted",
		"record_id":deleteOne.Id,
	})
}

