package controllers

import (
	"github.com/jackc/pgx/v4"
)

var dbConnect *pgx.Conn

// this function is so that we don't have to request to connect to the databse again and again
func InitiateDB(db *pgx.Conn) {
	dbConnect = db
}
