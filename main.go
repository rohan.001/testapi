package main

import (
	"log"
	"task/config"
	"task/routes"

	"github.com/gin-gonic/gin"
)

func main() {
	config.Connect()
	//Init router
	r := gin.Default()

	routes.Routes(r)
	log.Fatal(r.Run(":8080"))
}
