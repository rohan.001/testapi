package routes

import (
	"net/http"
	"task/controllers"

	"github.com/gin-gonic/gin"
)

func Routes(r *gin.Engine) {
	r.GET("/", welcome)
	r.GET("/all", controllers.Displayall)
	r.GET("/:testId", controllers.DisplayOne)
	r.POST("/insertOne",controllers.InsertOne)
	r.POST("/updateOne",controllers.UpdateOne)
	r.DELETE("deleteOne",controllers.DeleteOne)
}

func welcome(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status":  200,
		"message": "it works maybe ",
	})
}
